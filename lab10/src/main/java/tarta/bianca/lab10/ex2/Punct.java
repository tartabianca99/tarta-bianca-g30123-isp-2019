package tarta.bianca.lab10.ex2;

public class Punct {
    int x,y;

    public Punct(int x,int y){
        this.x=x;
        this.y=y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
