import tarta.bianca.lab10.ex2.FirGet;
import tarta.bianca.lab10.ex2.FirSet;
import tarta.bianca.lab10.ex2.Punct;

public class Ex2_TestSincronizare {
    public static void main(String[] args) {
        Punct p = new Punct(1,2);
        FirSet fs1 = new FirSet(p);
        FirGet fg1 = new FirGet(p);

        fs1.start();
        fg1.start();
    }
}