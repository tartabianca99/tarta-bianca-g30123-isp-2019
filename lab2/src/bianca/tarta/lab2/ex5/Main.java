package bianca.tarta.lab2.ex5;
import java.util.Scanner;

public class Main {
    public static void bubblesort(int v[], int N) {
        int i, j;
        for (i = 0; i < N - 1; i++)
            for (j = 0; j < N - i - 1; j++)
                if (v[j] > v[j + 1]) {
                    int aux = v[j];
                    v[j] = v[j + 1];
                    v[j + 1] = aux;
                }
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Nr. de elemente=");
        int N = s.nextInt();
        int v[] = new int[N];
        for (int i = 0; i < N; i++)
            v[i] = 1 + (int) (Math.random() * 100);
        for (int i = 0; i < N; i++) {
            System.out.println("v[" + i + "]=" + v[i]);
            bubblesort(v, N);
            System.out.println("Vectorul sortat:");
            for (i = 0; i < N; i++) {
                System.out.println("v[" + i + "]=" + v[i]);

            }

        }
    }
}
