package bianca.tarta.lab2.ex6;
import java.util.Scanner;

public class Main {
    public static int factorial_nerecursiv(int N,int f) {
        for(int i=1;i<=N;i++)
            f*=i;
        return f;
    }
    public static int factorial_recursiv(int N,int f) {
        if(N==1) return f;
        else return factorial_recursiv(N-1,f*N);
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("N=");
        int N = s.nextInt();
        int f=1;
        System.out.println("N!="+factorial_recursiv(N,f));
        System.out.println("N!="+factorial_nerecursiv(N,f));
    }

}
