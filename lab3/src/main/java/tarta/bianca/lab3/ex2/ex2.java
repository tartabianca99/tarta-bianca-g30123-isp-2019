package tarta.bianca.lab3.ex2;

public class ex2 {
    public class Circle{
        private double radius=1.0;
        private String color="red";
        public Circle(){
        }
        public Circle(String color){
            this.color=color;
        }
        public double getRadius(){
            return this.radius;
        }

        public double getArea() {
            return this.radius*this.radius*Math.PI;
        }
    }
}
