package tarta.bianca.lab3.ex4;

public class Mypoint {
    int x,y;
    public Mypoint(){
        this.x=0;
        this.y=0;
    }

    public Mypoint(int x,int y)
    {
        this.x=x;
        this.y=y;
    }

    public void setCoord(int x,int y){
        this.x=x;
        this.y=y;
    }

    public int getX()
    {
        return this.x;
    }

    public int getY()
    {
        return this.y;
    }

    public String toString(){
        return "("+this.x+","+this.y+")";

    }

    public double getDistance(int x,int y){
        return Math.sqrt(Math.pow((this.x-x),2)+Math.pow((this.y-y),2));
    }

    public double getDistance(Mypoint p1){
        return Math.sqrt(Math.pow((this.x-p1.getX()),2)+Math.pow((this.y-p1.getY()),2));
    }



}
