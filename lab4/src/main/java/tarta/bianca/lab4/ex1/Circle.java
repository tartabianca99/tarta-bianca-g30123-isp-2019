package tarta.bianca.lab4.ex1;

public class Circle {
    private double radius;
    private String color;

    public Circle (double radius) {
        this.radius = radius;
    }

    public Circle () {
        this.radius = 1.0;
        this.color = "red";
    }


    public double getRadius () {
        return radius;
    }

    public double getArea () {
        return this.radius * this.radius * Math.PI;
    }
}