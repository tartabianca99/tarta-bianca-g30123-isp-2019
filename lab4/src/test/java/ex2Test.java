import tarta.bianca.lab4.ex2.Author;
import org.junit.Test;
import org.junit.Assert;

import static org.junit.Assert.assertEquals;
public class ex2Test {
    @Test
    public void testAuthor(){
        Author myAuthor = new Author ("Bianca","tartabianca99@yahoo.es",'f');
        assertEquals (" Bianca(f) at tartabianca99@yahoo.es",myAuthor.toString());
    }
}
