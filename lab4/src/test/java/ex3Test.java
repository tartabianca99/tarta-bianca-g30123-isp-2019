import tarta.bianca.lab4.ex2.Author;
import tarta.bianca.lab4.ex3.Book;
import org.junit.Test;
import org.junit.Assert;

import static org.junit.Assert.assertEquals;

public class ex3Test {
    @Test
    public void test(){
        Author myAuthor = new Author ("Bianca","tartabianca99@yahoo.com",'f');
        Book myBook= new Book ("Carte1",myAuthor,35.5);
        assertEquals ("Book-Carte1 by  Bianca(f) at tartabianca99@yahoo.com",myBook.toString ());
    }
}
