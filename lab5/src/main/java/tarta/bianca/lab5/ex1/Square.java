package tarta.bianca.lab5.ex1;

public class Square extends Rectangle {
    public Square () {
        super();
    }

    public Square (double side) {
        super (side, side);
    }

    public Square (String color, boolean fillded, double side) {
        super (color, fillded, side, side);
    }

    public double getSide(){
        return this.width;
    }
    public void setSide(double side){
        this.width=side;
        this.length=side;
    }
    public void setWidth(double side){
        this.width=side;
        this.length=side;
    }
    public void setLength(double side){
        this.width=side;
        this.length=side;
    }
    public String toString(){
        return "Square with side: "+this.width+" ,color: "+this.color+" ,filled: "+this.filled;
    }
}