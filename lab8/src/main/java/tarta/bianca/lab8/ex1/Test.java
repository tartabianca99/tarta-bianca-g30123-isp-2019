package tarta.bianca.lab8.ex1;

public class Test {
    public static void main(String[] args){
        Persoana p1 = new Persoana("A", 10);
        ContBancar c = new ContBancar(p1, 0.1);

        c.detalii();
        c.depunere(200);
        c.actualizare();
        c.detalii();

        ManagerConturi mc = new ManagerConturi();

        Persoana a = new Persoana("Alina",23);
        Persoana b = new Persoana("Diana",18);
        Persoana e = new Persoana("Mihai",29);

        mc.creareCont(a, 0.05);
        mc.creareCont(b, 0.06);
        mc.creareCont(e, 0.04);

        mc.afiseaza();
    }
}
